import * as Yup from 'yup';

export const validation = (inputName1, inputName2) => 
    Yup.object().shape(getValidationSchema(inputName1, inputName2));

const getValidationSchema = (inputName1, inputName2) => {
    const validations = [{
      phone: Yup.string()
        .required('Enter your phone number')
        .matches(/^(\d+-?)+\d+$/, 'Must use numbers only or only with dash')
        .length(10 || 12, 'Phone number must be 10 digits or 12 characters with dash ')
        // when 12, what if input is all number?
        .trim('Remove sapce.'),
      }, {
      email: Yup.string()
        .required('Enter your email')
        .email('Enter email format')
        .trim('Remove space'),
    }, {
      dateOfBirthConfirm: Yup.bool()
        .oneOf([true], 'Must confirm your birthday'),
    }, {
      firstName: Yup.string()
          .required('Enter your your first name')
          .matches(/[a-zA-Z]/, 'Must use alphabetic letters')
          .trim('Remove sapce.'),
    }, {
      lastName: Yup.string()
          .required('Enter your last name')
          .matches(/[a-zA-Z]/, 'Must use alphabetic letters')
          .trim('Remove sapce.'),
    },{
      password: Yup.string()
          .required('Enter your password')
          .min(6, 'Must be greater than 6 letters.') // later on combination
          .trim('Remove sapce.'),
    }]; 
    
      const single_validation = validations.find(validation => validation[inputName1]);
      
      if(inputName2) {
        const double_validation = validations.find(validation => validation[inputName2]);

        if(!double_validation) {
          throw new Error('Unable to validate the current input')
        }

        return { ...single_validation, ...double_validation };
      }
  
      if(!single_validation) {
        throw new Error('Unable to validate the current input')
      }
  
      return single_validation;
}