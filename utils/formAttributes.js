export const formAttributes = [{
  name: 'phone',
  label: 'Phone',
  placeholder: 'Phone Number',
}, { 
  name: 'email',
  label: 'Email',
  placeholder: 'Email Address'
}, {
  name: 'dateOfBirthConfirm',
  birthdays: [{
    name: 'birthDayMonth',
    label: 'Month'
  }, {
    name: 'birthDayDate',
    label: 'Date'
  }, {
    name: 'birthDayYear',
    label: 'Year'
  }],
  label: 'Birthday 🎉'
}, {
  name: 'firstName',
  label: 'First Name',
  placeholder: 'First Name'
}, {
  name: 'lastName',
  label: 'Last Name',
  placeholder: 'Last Name'
}, {
  name: 'password',
  label: 'Password',
  placeholder: 'Password'
}, {
  name: 'confirmation',
  label: 'Confirmation Code',
  verificationInputs: [{
    name: 'firstCode'
  }, {
    name: 'secondCode'
  }, {
    name: 'thirdCode'
  }, {
    name: 'forthCode'
  }]
}];

export const monthNames = [ 
  { label: 'January', value: '1' }, { label: 'February', value: '2' }, { label: 'March', value: '3' },
  { label: 'April', value: '4' }, { label: 'May', value: '5' }, { label: 'June', value: '6' },
  { label: 'July', value: '7' }, { label: 'August', value: '8' }, { label: 'September', value: '9' },
  { label: 'October', value: '10' }, { label: 'November', value: '11' }, { label: 'December', value: '12' }
];
    