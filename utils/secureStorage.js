import { ACCESS_CONTROL, ACCESSIBLE, AUTHENTICATION_TYPE } from 'react-native-secure-storage';
 
export const config = {
    accessControl: ACCESS_CONTROL.BIOMETRY_CURRENT_SET_OR_DEVICE_PASSCODE ,
    accessible: ACCESSIBLE.WHEN_UNLOCKED,
    authenticationPrompt: 'Authentication with yourself',
    service: 'example',
    authenticateType: AUTHENTICATION_TYPE.BIOMETRICS,
  }

export const key = {
    token: 'token'
};

