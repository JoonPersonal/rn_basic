import React from 'react'

import  { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { ApolloLink } from 'apollo-link';
import { ApolloProvider } from 'react-apollo';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import reduxThunk from 'redux-thunk';
import reducers from './App/Redux/reducers/index';

import NavigationRouter from './App/Components/NavigationRouter'
import Theme from './App/Theme/Theme'
// import { Header } from './App/Components/Credentials/Signup/SampleFromJoon/Common'


import styled, { ThemeProvider } from 'styled-components/native'

const client = new ApolloClient({
  link: ApolloLink.from([
    onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors)
        graphQLErrors.forEach(({ message, locations, path }) =>
          console.log(
            `[GraphQL error]: Message: ${ message }, Location: ${ locations }, Path: ${ path }`,
            ),
          );
          if (networkError) console.log(`[Network error]: ${networkError}`);
        }),
        new HttpLink({
          uri: 'http:/192.168.0.103:4000/graphql',
          credentials: 'same-origin'
        })
      ]),
      cache: new InMemoryCache()
  });
      
const store = createStore(reducers, applyMiddleware(reduxThunk));

const App = () => {
  return (
    <ApolloProvider client={ client } >
      <Provider store={ store } >
        <ThemeProvider theme={ Theme }>
          <StyledPage>
            {/* <Header /> */}
            <NavigationRouter />
          </StyledPage>
        </ThemeProvider>
      </Provider>
    </ApolloProvider>
  );
}

const StyledPage = styled.View`
  position: absolute;
  top: 0;right: 0;bottom: 0; left: 0;
`
export default App;