import {
  AUTH_ONCHANGE
} from './types'

export const authChange = onchange => {
  return {
    type: AUTH_ONCHANGE,
    payload: onchange
  };
};

