import { combineReducers } from 'redux';
import userInputs from './handleOnChange';

export default combineReducers({ 
  inputState: userInputs
});