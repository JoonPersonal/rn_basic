import {
  AUTH_ONCHANGE,
} from '../actions/types';

const initialState = {
  dateOfBirthConfirm: false
}

const handleOnChange = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_ONCHANGE:
      const { name, value } = action.payload;
      return { ...state, [name]: value }
  }
  return state;
}

export default handleOnChange;