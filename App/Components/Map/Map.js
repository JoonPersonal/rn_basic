import React, { Component } from 'react'
import { View, StyleSheet, Dimensions, Image } from 'react-native'
import * as Permissions from 'expo-permissions'
import MapView from 'react-native-maps'
import MapStyle from './GoogleMapsStyle'
// import Tongue from '../Tongue/Tounge'
import ToungeComponents from '../Tongue/ToungeComponents'

import ProfileMarker from './MapMarkers/ProfileMarker/ProfileMarker'
import BarMarker from './MapMarkers/BarMarker/BarMarker'
const { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height

const LATITUDE_DELTA = 0.0092
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO
export default class Map extends Component {
  constructor(props) {
    super(props)
    this.state = {
      markerPosition: {
        latitude: 45.4616148,
        longitude: -89.4882664
      },
      barmarkerPosition: {
        latitude: 43.4425156,
        longitude: -80.4882664
      },
      initialPosition: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      }
    }
    watchID = null
  }
  async componentDidMount() {
    this._getLocationAsync()
  }
  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID)
  }
  _getLocationAsync = async () => {
    const { status, permissions } = await Permissions.askAsync(Permissions.LOCATION)
    if (status === 'granted') {
      await navigator.geolocation.getCurrentPosition(
        this.geoSuccess,
        (error) => alert(JSON.stringify(error)),
        { enableHighAccuracy: true, timeout: 4000, maximumAge: 1000 })
      this.watchID = navigator.geolocation.watchPosition((position) => {
        const latitude = parseFloat(position.coords.latitude)
        const longitude = parseFloat(position.coords.longitude)
        var lastRegion = {
          latitude,
          longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA
        }
        this.setState({ initialPosition: lastRegion })
        this.setState({ markerPosition: lastRegion })
        // console.log(status, permissions)
      },
        (error) => alert(JSON.stringify(error)),
        { enableHighAccuracy: true, timeout: 4000, maximumAge: 1000 }
      )
    } else {
      throw new Error('Hey we need your location to show bars near you. :)');
    }
  }
  geoSuccess = (position) => {
    const latitude = parseFloat(position.coords.latitude)
    const longitude = parseFloat(position.coords.longitude)
    // console.log('HERE IS THE NEXT PEOP', latitude ,longitude)
    var initialPosition = {
      latitude: latitude,
      longitude: longitude,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA
    }
    this.setState({
      initialPosition,
    });
    this.setState({
      markerPosition: initialPosition
    });
    console.log(initialPosition)
  }
  onRegionChange(region) {
    this.setState({ region })
  }
  render() {
    return (
      <View style={{ position: 'absolute', bottom: 0, top: -10, left: 0, right: 0 }}>
        <MapView.Animated
          style={styles.map}
          provider={MapView.PROVIDER_GOOGLE} // remove if not using Google Maps
          customMapStyle={MapStyle}
          region={this.state.initialPosition}
          tracksInfoWindowChanges={false}
          tracksViewChanges={false}
        >
        <BarMarker coordinate={this.state.barmarkerPosition}  />
        <ProfileMarker coordinate={this.state.markerPosition} />
        </MapView.Animated>
        <ToungeComponents />
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
})


















//   componentDidMount() {
//     this.watchID = navigator.geolocation.watchPosition( this.geoSuccess, this.geoFailure,
//       { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
//     );
//   }
//   geoSuccess = (position) => {
//     const { coordinate } = this.state;
//     const { latitude, longitude } = position.coords;

//     const newCoordinate = {
//       latitude,
//       longitude
//     };
//     if (Platform.OS === "android" || Platform.OS === "ios") {
//       if (this.marker) {
//         this.marker._component.animateMarkerToCoordinate(
//           newCoordinate,
//           500
//         );
//       }
//     } else {
//       coordinate.timing(newCoordinate).start();
//     }
//     this.setState({
//       latitude,
//       longitude,
//       prevLatLng: newCoordinate
//     });
//   }
// geoFailure = error => {
//   error => console.log(error)
// }
//   getMapRegion = () => ({
//     latitude: this.state.latitude,
//     longitude: this.state.longitude,
//     latitudeDelta: LATITUDE_DELTA,
//     longitudeDelta: LONGITUDE_DELTA
//   });