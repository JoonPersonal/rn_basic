import React, { Component } from 'react'
import { View, Dimensions,Image } from 'react-native'
import styled from 'styled-components/native'
import { Actions } from 'react-native-router-flux'
import { Marker }  from 'react-native-maps'
import { LinearGradient } from 'expo-linear-gradient';
import picture from '../../../../../deletableAssests/me.png'


const { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
// const LATITUDE = 43.45405704971119
// const LONGITUDE = -80.49266117442808

const LATITUDE_DELTA = 0.0092
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO


const ProfilePicture = styled.Image`
  /* background: #000; */
  align-self: center;
  width: 57;
  height: 57;
  margin-top: 3.5;
  border-radius: 28;
  z-index: 10;
`;

export default class ProfileMarker extends Component {
  constructor(props) {
    super(props)
    this.state = {
      latitude: this.props.coordinate.latitude,
      longitude: this.props.coordinate.longitude,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA
    }
  }
  render() {
    return (
       <Marker coordinate={this.props.coordinate}
            onPress={() => Actions.profile()}
            tracksInfoWindowChanges={false}
            // tracksViewChanges={false}
            >
            <View style={{ marginLeft: 25 }}>
              <LinearGradient
              style={{  height: 64, width: 64, borderRadius: 32 }}
              // colors={['#0053FF', '#7400FF']}
              colors={['#ff7777', '#714999']}
              >
                <ProfilePicture
                  // style={{ alignSelf: 'center', width: 55, height: 55, borderRadius: 25, marginTop: 4.5 }}
                  source={picture}
                  />
              </LinearGradient>
              </View>
          </Marker>
  )}
}
