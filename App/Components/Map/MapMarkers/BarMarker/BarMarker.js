import React, { Component } from 'react'
import { View, Dimensions, Image } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { Marker }  from 'react-native-maps'
import { LinearGradient } from 'expo-linear-gradient';
import picture from '../../../../../deletableAssests/me.png'
import gql from 'graphql-tag'
import { Query } from 'react-apollo'


// export const GET_BAR_QUERY = gql`
//   query GET_BARS_QUERY {
//     bars {
//       id
//       name
//       coordinates {
//         latitude
//         longitude
//       }
//       barprofile {
//         established
//       }
//     }
//   }
// `

export default class BarMarker extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
    // <Query
    //   query={GET_BAR_QUERY}
    // >
      <Marker coordinate={this.props.coordinate}
        // tracksViewChanges={false}
        onPress={() => Actions.profile()}
      >
        <View style={{ marginLeft: 25 }}>
          <LinearGradient
            style={{ height: 64, width: 64, borderRadius: 32 }}
            colors={['#ff3333', '#341494']}
          >
            <Image
              style={{ 
                // backgroundColor: 'blue',
                zIndex: 100,
                alignSelf: 'center', width: 55, height: 55, borderRadius: 25, marginTop: 4.5 }}
                source={{picture}}
            />
          </LinearGradient>
        </View>
      </Marker>
    // </Query>
  )}
}
