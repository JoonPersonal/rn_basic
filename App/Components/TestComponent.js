import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity
} from "react-native"
import { Actions } from 'react-native-router-flux'

class TestComponent extends Component {
  render() {
    return (
      <View>
        <Text
          style={{
            marginTop: 50,
            fontWeight: '600',
            textAlign: 'center',
          }}
        > THIS IS A TEST COMPONENT GO WILD </Text>
        <View
          style={{
            backgroundColor: '#1d1d1d',
            height: '100%',
            color: '#ffffff'
          }}
        >
          <View style={[styles.container]}>
            <View style={{ width: '100%' }}>
              <View
                style={{
                  flexDirection: 'row',
                  height: 50,
                  alignItems: 'center',
                }}
              >
                <TouchableOpacity
                  onPress={() => Actions.signupflow()}
                  style={{
                    flex: 1,
                    alignItems: 'stretch',
                  }}
                >
                  <Text
                    style={{
                      textAlign: 'center',
                      fontSize: 20,
                      color: 'white'
                    }}
                  >
                    View Signup -- REDUX
                    </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  height: 50,
                  alignItems: 'center',
                }}
              >
                <TouchableOpacity
                  onPress={() => Actions.profile()}
                  style={{
                    flex: 1,
                    alignItems: 'stretch',

                  }}
                >
                  <Text
                    style={{
                      textAlign: 'center',
                      fontSize: 20,
                      color: 'white'
                    }}
                  >
                    View Profile
                    </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }
}
export default TestComponent

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    color: 'white'
  }
});