
import React, { Component } from 'react';
import { Animated, StyleSheet, View, Dimensions, Text, TouchableOpacity } from 'react-native';
import styled from 'styled-components/native'
import {
  PanGestureHandler,
  NativeViewGestureHandler,
  State,
  TapGestureHandler,
} from 'react-native-gesture-handler';
import PropTypes from 'prop-types';


const CurrentText = styled.Text`
    text-align: center;
    height: 25px;
    margin-top: 10px;
    font-weight: 400;
    line-height: 25px;
    text-transform: lowercase;
    color: ${props => props.theme.informationColor};
    font-size: ${props => props.theme.informationFontSize};
  `;

const USE_NATIVE_DRIVER = true
const HEADER_HEIGHT = 50
const windowHeight = Dimensions.get('window').height
const SNAP_POINTS_FROM_TOP = [0, windowHeight * 0.4, windowHeight * 0.75]

export default class Tounge extends Component {
  masterdrawer = React.createRef();
  drawer = React.createRef();
  drawerheader = React.createRef();
  scroll = React.createRef();
  
  constructor(props) {
    super(props);
    const START = SNAP_POINTS_FROM_TOP[0]
    const END = SNAP_POINTS_FROM_TOP[SNAP_POINTS_FROM_TOP.length - 1]
    this.state = {
      lastSnap: END,
      name: 'Christian',
      yValue: HEADER_HEIGHT
    }

    this._lastScrollYValue = 0
    this._lastScrollY = new Animated.Value(0)

    this._onRegisterLastScroll = Animated.event(
      [{ nativeEvent: { contentOffset: { y: this._lastScrollY } } }]
    )
    this._lastScrollY.addListener(({ value }) => {
      this._lastScrollYValue = value
    })

    this._dragY = new Animated.Value(0)
    this._onGestureEvent = Animated.event(
      [{ nativeEvent: { translationY: this._dragY } }],
      { useNativeDriver: USE_NATIVE_DRIVER },
      { useNativeDriver: USE_NATIVE_DRIVER }
    )

    this._reverseLastScrollY = Animated.multiply(
      new Animated.Value(-1),
      this._lastScrollY
    )

    this._translateYOffset = new Animated.Value(END)

    this._translateY = Animated.add(
      this._translateYOffset,
      Animated.add(this._dragY, this._reverseLastScrollY)
    ).interpolate({
      inputRange: [START, END],
      outputRange: [START, END],
      extrapolate: 'clamp',
    })
  }
  _onHeaderHandlerStateChange = ({ nativeEvent }) => {
    if (nativeEvent.oldState === State.BEGAN) {
      this._lastScrollY.setValue(0)
    }
    this._onHandlerStateChange({ nativeEvent })
  }

  _onHandlerStateChange = ({ nativeEvent }) => {
    // console.log(nativeEvent)
    if (nativeEvent.oldState === State.ACTIVE) {
      let { velocityY, translationY } = nativeEvent
      translationY -= this._lastScrollYValue
      // console.log(translationY)
      const dragToss = 0.091
      const endOffsetY = this.state.lastSnap + translationY + dragToss * velocityY

      let destSnapPoint = SNAP_POINTS_FROM_TOP[0]
      for (let i = 0; i < SNAP_POINTS_FROM_TOP.length; i++) {
        const snapPoint = SNAP_POINTS_FROM_TOP[i]
        const distFromSnap = Math.abs(snapPoint - endOffsetY)

        if (distFromSnap < Math.abs(destSnapPoint - endOffsetY)) {
          destSnapPoint = snapPoint;
        }
      }
      this.setState({ lastSnap: destSnapPoint })
      this._translateYOffset.extractOffset()
      this._translateYOffset.setValue(translationY)
      this._translateYOffset.flattenOffset()
      this._dragY.setValue(0)
      Animated.spring(this._translateYOffset, {
        velocity: velocityY,
        tension: 28,
        friction: 12,
        toValue: destSnapPoint,
        useNativeDriver: USE_NATIVE_DRIVER,
      }).start()
    }
  }
  translateMyTounge() {
    // Animated.spring(this._translateYOffset, {
    //   velocity: 25,
    //   tension: 28,
    //   friction: 12,
    //   toValue: 819,
    //   useNativeDriver: USE_NATIVE_DRIVER,
    // }).start()

    // console.log(this._translateYOffset = )
  }

  render() {
    return (
      <TapGestureHandler
        maxDurationMs={100000}
        ref={this.masterdrawer}
        maxDeltaY={this.state.lastSnap - SNAP_POINTS_FROM_TOP[0]}
      >

        <View style={StyleSheet.absoluteFillObject} pointerEvents="box-none">
          <Animated.View
            style={[
              StyleSheet.absoluteFillObject,
              {
                transform: [{ translateY: this._translateY }],
              },
            ]}
          >
            <PanGestureHandler
              ref={this.drawerheader}
              simultaneousHandlers={[this.scroll, this.masterdrawer]}
              shouldCancelWhenOutside={false}
              onGestureEvent={this._onGestureEvent}
              onHandlerStateChange={this._onHeaderHandlerStateChange}>
              <Animated.View style={[styles.tounge, { height: this.state.yValue }]} />
            </PanGestureHandler>

            <PanGestureHandler
              ref={this.drawer}
              simultaneousHandlers={[this.scroll, this.masterdrawer]}
              shouldCancelWhenOutside={false}
              onGestureEvent={this._onGestureEvent}
              onHandlerStateChange={this._onHandlerStateChange}
            >
              <Animated.View style={styles.container}>
                <NativeViewGestureHandler
                  ref={this.scroll}
                  waitFor={this.masterdrawer}
                  simultaneousHandlers={this.drawer}
                >
                  <Animated.ScrollView
                    style={[
                      styles.scrollView,
                      { marginTop: 20, 
                        // backgroundColor: 'blue',
                         marginBottom: SNAP_POINTS_FROM_TOP[0] },
                    ]}
                    bounces={false}
                    onScrollBeginDrag={this._onRegisterLastScroll}
                    scrollEventThrottle={1}
                  >
                    {this.props.children}

                  </Animated.ScrollView>
                </NativeViewGestureHandler>
              </Animated.View>
            </PanGestureHandler>
          </Animated.View>
        </View>
      </TapGestureHandler>
    )
  }
}
// User.propTypes = {
//   children: PropTypes.func.isRequired,
// };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#080808',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    zIndex: 100
  },
  tounge: {
    // backgroundColor: 'red',
    marginTop: 30,
    height: HEADER_HEIGHT,
    zIndex: 100
  },
})