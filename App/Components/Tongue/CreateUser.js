import React, { Component } from 'react';
import gql from "graphql-tag"
import { Mutation } from "react-apollo"
import  { StyleSheet, Text, View, Button, TextInput } from 'react-native'

const CREATE_USER_MUTATION = gql`
  mutation CREATE_USER_MUTATION {
    createuser{
    id
    token
  }
  }
`;

export default class CreateUser extends Component {
  state = {
    email: '',
    password: '',
    name: '',
    resultRegister: '',
    resultLogin: ''
  };

  render() {
    return (
        <View style={styles.container}>
          <Mutation mutation={CREATE_USER_MUTATION}>
            {(createuser, { data }) => (
              <View>
              <Button
                  onPress={ () => {
                    createuser()
                      .then(res => {
                        console.log(
                          "result",
                          res
                        );
                        return res;
                      })
                      .catch(err => <Text>{err}</Text>);
                  }}
                  title="Register user"
                />
                <Text>{this.state.resultRegister}</Text>
              </View>
            )}
          </Mutation>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5F'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  input: {
    height: 30,
    width: 150,
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 5,
    padding: 1
  }
});