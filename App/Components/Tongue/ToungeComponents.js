import React, { Component } from 'react'
import styled from 'styled-components/native'
import { Text, View } from 'react-native'
import Tounge from './Tounge'
import Search from '../Search/Search'
import Signupfriends from '../Credentials/Authentications/SignUpfriends'
import Signuphome from '../Credentials/Authentications/SignUphome'
import CreateUser from './CreateUser'

const CurrentText = styled.Text`
  text-align: center;
  height: 25px;
  font-weight: 400;
  line-height: 25px;
  text-transform: lowercase;
  color: ${props => props.theme.informationColor};
  font-size: ${props => props.theme.h3};
`;
export  default  class ToungeComponents extends Component {
  constructor(props) {
    super(props) 
    this.state = {
      name: "Welcome"
    }
  }
  render() {
    return (
      <Tounge >
        <CurrentText>{this.state.name}</CurrentText>
        <Search />
        <Signupfriends/>
        <Signuphome />
        {/* <Signup /> */}
        {/* <SignupUser /> */}
        {/* <CreateUser /> */}
      </Tounge>
    )
  }
}


{/* <TouchableOpacity onPress={()=> this.translateMyTounge()} style={{width:100,height:100,backgroundColor:'red'}}>
  <Text style={{color:'white'}}>
    hello
  </Text>
</TouchableOpacity>  */}
{/* <User> */}
  {/* {({ data }) => {
    const me = data ? data.me : null
    return (
      <View>
        {!me && (
          <Signup />
        )
        }
        {me && (
          <View style={{background: 'red'}}>
            <Text>email</Text>
          </View>
        )}
      </View>
    )
  }} */}
{/* </User> */}