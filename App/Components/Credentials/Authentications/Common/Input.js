import React from 'react';
import { View } from 'react-native';
import styled from 'styled-components';

 const AuthInput = styled.TextInput`
  background: red;
  width: 95%;
  align-self: center;
 `;

const Input = ({ label, value, keyboardtype, onChangeText, 
  placeholder, autoFocus, onBlur, secureTextEntry, maxLength }) => {
    
  return (
    <React.Fragment>
      <AuthInput 
        // Do we need to define css things here?
        textColor="#FFFFFF"
        baseColor="#FFFFFF"
        tintColor="#FFFFFF"
        errorColor="#FF2323"
        fontSize={ 25 }
        labelFontSize= {18 }
        labelHeight={ 0 }
        keyboardAppearance="dark"
        autoCapitalize="none"
        autoFocus = { autoFocus }
        keyboardType={ keyboardtype }
        returnKeyType="done"
        label={ label }
        value={ value }
        placeholder={ placeholder }
        onChangeText={ onChangeText }
        onBlur={ onBlur }
        secureTextEntry= { secureTextEntry || false } 
        maxLength={ maxLength || null }
      />
    </React.Fragment>);
}

export { Input };