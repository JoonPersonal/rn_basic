import React from 'react';
import CheckBox from 'react-native-check-box'

const CheckInput = ({ onClick, isChecked, leftText, leftTextStyle, checkBoxColor }) => {
    return(
        <React.Fragment>
            <CheckBox
                onClick={ onClick }
                isChecked={ isChecked }
                leftText={ leftText || '' } 
                leftTextStyle={ leftTextStyle }
                checkBoxColor={ checkBoxColor }              
            />
        </React.Fragment>        
    )
}

export { CheckInput };