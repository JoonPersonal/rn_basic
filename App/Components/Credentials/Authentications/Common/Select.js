import React from 'react';
import { View, Picker, Text } from 'react-native';

const Select = props => {
    const { onValueChange, selectedValue, title, itemList, prompt } = props;
    
    const renderPickerItems = () => {
        if(props.itemList && props.itemList.length > 0) {
            return itemList.map((item, index) => {
                return <Picker.Item 
                    key={ index } 
                    label={ item.label } 
                    value={ item.value } 
                />
            })
        } else {
            return null;
        }
    }

    return(
        <React.Fragment>
            <Text style={ styles.title }>{ title && title }</Text>
            <Picker
                style={ styles.twoPickers }
                itemStyle={ styles.pickerItem }
                onValueChange={ onValueChange }
                selectedValue={ selectedValue }
            >
                { renderPickerItems() }    
            </Picker>
        </React.Fragment>
    );
};

const styles = {
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        padding: 20,
        backgroundColor: 'white',
      },
      title: {
        // width: '25%',
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 10,
        color: 'white'
      },
      picker: {
        width: 200,
        backgroundColor: '#FFF0E0',
        borderColor: 'black',
        borderWidth: 1,
      },
      pickerItem: {
        color: 'red'
      },
      onePicker: {
        width: 200,
        height: 44,
        backgroundColor: '#FFF0E0',
        borderColor: 'black',
        borderWidth: 1,
      },
      onePickerItem: {
        height: 44,
        color: 'red'
      },
      twoPickers: {
        width: 100,
        height: 88,
        backgroundColor: '#FFF0E0',
        borderColor: 'black',
        borderWidth: 1,
      },
      twoPickerItems: {
        height: 88,
        color: 'red'
      },
}

export { Select };