import React from 'react'
import styled from 'styled-components'

const Btn = styled.TouchableOpacity`
  /* background: red; */
  background: #0083E4;
  height: 43px;
  width: 95%;
  align-self: center;
  border-radius: 5px;
`;

const BtnText = styled.Text`
  line-height: 43px;
  text-align: center;
  font-size: 16px;
  font-weight: 500;
  color: #ffffff;
`;

const Button = ({ onPress, children }) => {
    return(
        <Btn onPress={ onPress } >
          <BtnText>{ children }</BtnText>
        </Btn>
    );
}

// const styles = {
//     buttonStyle: {
//         // flex: 1,
//         // alignItem: control "children" styling
//         // position "itself" in flex box role
//         height: 56,
//         alignSelf: 'stretch',
//         backgroundColor: '#1d1d1d',
//         borderRadius: 5,
//         borderWidth: 1,
//         borderColor: '#1EED00',
//         marginLeft: 5,
//         marginRight: 5
//     },
//     textStyle: {
//         alignSelf: 'center',
//         color: '#FFFFFF',
//         fontSize: 16,
//         // use string
//         fontWeight: '600',
//         paddingTop: 10,
//         paddingBottom: 10
//     }
// }

export { Button };