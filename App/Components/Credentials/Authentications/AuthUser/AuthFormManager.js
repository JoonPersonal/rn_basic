import React from 'react';
import { View, Text } from 'react-native';

import { formAttributes } from '../../../../../utils/formAttributes';
import { Button, Input } from '../Common';


const getIndex = name => formAttributes.findIndex(attribute => 
    attribute.name === name);

export const handleNextButton = (name, setUserInput) => {

    const index = getIndex(name);
    if(index === 0 || index === 3) {
        setUserInput([ formAttributes[index+2].name ]);
        return;
    }

    if(index === 2) {
        setUserInput([formAttributes[3].name, formAttributes[4].name ]);
        return;
    }
    
    if(index !== 6) {
        setUserInput([ formAttributes[index+1].name ]);
    }
}

export const handleBackButton = (name, setUserInput, inputState) => {
    
    const index = getIndex(name);
    if (index === 2) {
        if(inputState.email) {
            setUserInput([ formAttributes[index-1].name ]);
            return;
        }
        setUserInput([ formAttributes[index-2].name ]);
        return;
    }

    if (index === 5) {
        setUserInput([ formAttributes[index-2].name, formAttributes[ index-1].name ]);
        return;
    }

    if (index !== 0) {            
        setUserInput([ formAttributes[index-1].name ]);
    }
}

// Made a reusable comonent
export const TwoTabButtons = ({ setContent, contents, resetData }) => {
    return (
        <View style={ flexStyles.tabs }>
            <View style={ flexStyles.items }>
                <Button onPress={ () => {
                    setContent([ contents[0] ]);
                    resetData && resetData({ name: contents[1], value: '' })
                }} >
                    Phone
                </Button>
            </View>
            <View style={ flexStyles.items }>
                <Button onPress={ () => { 
                    setContent([ contents[1] ]);
                    resetData && resetData({ name: contents[0], value: '' });
                }} >
                    Email
                </Button>
            </View>
        </View>
    );
}

export const getDateOfBirthCalendar = () => {
    const presentTime = new Date();
    return {
        current_month: presentTime.getMonth() + 1,
        current_date: presentTime.getDate(),
        current_year: presentTime.getFullYear()    
    }
}

// need to put somewhere in the structure
const flexStyles = {
    tabs: {
      flexDirection: 'row',
      width: '95%',
    //   height: 60,
      alignSelf: 'center',
      marginBottom: 20,
    
    },
    items: {
      flex: 1
    }
}