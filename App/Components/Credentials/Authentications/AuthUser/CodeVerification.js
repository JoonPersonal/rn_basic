import React, { useState } from 'react';
import { View, Text } from 'react-native';
import { Actions } from 'react-native-router-flux'
import { graphql } from 'react-apollo';
import * as SecureStore from 'expo-secure-store';

import { Input, Button } from '../Common';
import { formAttributes } from '../../../../../utils/formAttributes';
import mutation from '../../../../mutations/confirmUser';

export const CodeVerification = ({ inputState, userCode, authChange, mutate }) => {    
    
    const [ errorMessage, setErrorMessage ] = useState('');

    const { verificationInputs, label } = formAttributes[6];
    const { firstCode, secondCode, thirdCode, forthCode } = inputState;

    const userInputConfirmationCode = `${firstCode || ''}${secondCode || ''}${thirdCode || ''}${forthCode || ''}`
    
    const handleOnClick = async () => {
        let isVerified;
        if(userCode.smsCode !== userInputConfirmationCode) {
            setErrorMessage('Your input is not identical')
            isVerified = false;
            return;
        }
        isVerified = true;
        const response = await mutate({ variables: { isVerified }});
        const { token } = response.data.codeVerification;

        SecureStore.setItemAsync('token', token);
        const tokenReturned = await SecureStore.getItemAsync('token');
        console.log('token_response,', tokenReturned);

        Actions.profile();
    }

    const renderInputs = () => {
        return verificationInputs.map(input => {
            const { name } = input;
            
            return (<View key={ name } style={ flexStyles.items }>
                <Input
                    maxLength={1}
                    autoFocus={ name==='firstCode' && true }
                    value={ inputState[name] || '' }
                    keyboardtype={ 'number-pad' }
                    onChangeText={ text => authChange({ name, value: text }) }            
                />
            </View>);
        });
    }    

    return (
        <View>
            <View>
                <Text style={ flexStyles.title }>{ label }</Text>
            </View>
            <View style={ flexStyles.tabs } >
                { renderInputs() }
            </View>
            <Text style={ flexStyles.title }>{ errorMessage || '' }</Text>
            <Button
                onPress={ () => handleOnClick() }
            >Submit</Button>
        </View>
    );
}

export default graphql(mutation)(CodeVerification);

// need to put somewhere in the structure
const flexStyles = {
    tabs: {
      flexDirection: 'row',
      width: '95%',
    //   height: 60,
      alignSelf: 'center',
      marginBottom: 20,
    
    },
    items: {
      flex: 1
    }, 
    title : {
      color: 'white'
    }
}