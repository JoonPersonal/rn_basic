import React, { useEffect } from 'react';
import { View, Text } from 'react-native';

import { Select } from '../Common';
import { monthNames, formAttributes } from '../../../../../utils/formAttributes'
import { getDateOfBirthCalendar } from './AuthFormManager';

export const DateOfBirth = ({ authChange, inputState }) => {

    const { current_month, current_date, current_year } = getDateOfBirthCalendar();
    const max_year = current_year - 18;
    const min_year = max_year - 79;

    const months = monthNames
        .sort((min, max) => Number(max.value)-Number(min.value))
        .filter(month => {
            if(inputState['birthDayYear'] === max_year.toString()) {
                return Number(month.value) <= current_month; 
            }
            return Number(month.value) <= 12
        });
    
    const dates = [];
    const start_year = inputState['birthDayYear'] === max_year.toString();
    const start_month = inputState['birthDayMonth'] === current_month.toString();
    const start_date = start_year && start_month ? current_date : 31;    
    for(let i = start_date; 1 <= i; i--) {
        dates.push({label: i.toString(), value: i.toString()});
    }
    
    const years = []; 
    for(let i = max_year; min_year <= i; i--) {
        years.push({label: i.toString() , value: i.toString()});
    }

    const { label, birthdays } = formAttributes[2];
    
    return(
        <View style={ flexStyles.tabs }>
            <Text style={{ color: 'orange' }}>{ label }</Text>
            {
                birthdays.map((dob, index) => {
                    return(
                        <View key={ index } style={ flexStyles.items }>
                            <Select 
                                title={ dob.label }
                                itemList={ index === 0 && months || index === 1 && dates || index === 2 && years }
                                onValueChange={ birthday => authChange({ name: dob.name, value: birthday }) }
                                selectedValue={ inputState[dob.name] }
                            />
                        </View>
                    );
                })
            } 
        </View>
    );
}

export default DateOfBirth;

const flexStyles = {
    tabs: {
      flexDirection: 'row',
      width: '95%',
      alignSelf: 'center',
      marginBottom: 20,
    
    },
    items: {
      flex: 1
    }
}
