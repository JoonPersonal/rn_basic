import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import { Formik} from 'formik';
import { graphql } from 'react-apollo';

import { Button, Input, CheckInput } from '../Common';
import { authChange, signupUser } from '../../../../Redux/actions';
import { formAttributes, validation } from '../../../../../utils';
import { handleNextButton, handleBackButton, TwoTabButtons, getDateOfBirthCalendar } from './AuthFormManager';
import DateOfBirth from './DateOfBirth';
import CodeVerification from './CodeVerification';
import mutations from '../../../../mutations/signupUser';

const AuthForm = ({ authChange, inputState, mutate, signupInput }) => {

  const { current_month, current_date, current_year } = getDateOfBirthCalendar();

  const [ userInput, setUserInput ] = useState([ 'phone' ]);
  const [ userCode, setUserCode ] = useState({});
  
  useEffect(() => {
    const max_year = current_year - 18;
    if(!inputState.dateOfBirth) {
      authChange({ name: 'birthDayMonth', value: current_month.toString() });
      authChange({ name: 'birthDayDate', value: current_date.toString() });
      authChange({ name: 'birthDayYear', value: max_year.toString() });
    }

    if(userCode.smsCode) {
      setUserInput(['confirmation']);
    }

  }, [userCode.smsCode]);

  const renderInputs = formAttributes.filter(input => userInput.indexOf(input.name) > -1);
  const buttonName = userInput[0] === 'password' || userInput[0] === 'confirmation' ? 'Submit' : 'Next';
  const isEmailPhone = userInput[0] === 'phone' || userInput[0] === 'email';

  return (
    <FormView> 
      <View style={{ height: '100%', marginTop: 150 }}>
        {      
          (renderInputs[0].name === 'phone' || renderInputs[0].name === 'email' ) && (<TwoTabButtons 
            setContent = { setUserInput }
            contents = { [ 'phone', 'email' ]  }
            resetData={ authChange }
          />)    
        }
        {
          renderInputs[0].name !== 'confirmation' && (
            <Formik
              initialValues={{}}
              validationSchema={ validation(renderInputs[0].name, renderInputs[1] && renderInputs[1].name) } 
              onSubmit={ async () => {
                if(renderInputs[0].name === 'password') {
                  const response = await mutate({ variables: { ...signupInput }});
                  const { smsCode, id } = response.data.verificationCodeCreation;
                  setUserCode({ smsCode, id });                  
                } else {
                  handleNextButton(renderInputs[0].name, setUserInput);
                }
              }}
            >     
                { formikProps => {

                  const { values, setFieldTouched, handleChange, handleSubmit, touched, errors } = formikProps;
                  
                  for (const key in values) {
                        delete values[key];
                  }
                  
                  values[ renderInputs[0].name ] = inputState[ renderInputs[0].name ];
                  if (renderInputs[1]) values[ renderInputs[1].name ] = inputState[ renderInputs[1].name ];

                  return(
                    <View>                    
                      { 
                        (renderInputs[0].name !== 'dateOfBirthConfirm' && renderInputs[0].name !== 'confirmation') && renderInputs.map(input => 
                          (<View key={ input.name }>
                            <Input    
                              autoFocus={ input.name !== 'lastName' && true }
                              label={ input.label }
                              value={ inputState[input.name] || ''}
                              keyboardtype={ input.name === 'phone' ? 'number-pad' : 'default' }
                              placeholder={ input.placeholder || undefined }
                              onChangeText={ text => {
                                authChange({ name: input.name, value: text });
                                handleChange(text);
                              }}
                              onBlur={ () => setFieldTouched(input.name) }
                              secureTextEntry={ input.name === 'password' && true }
                            />
                            {
                              touched[input.name] && errors[input.name] && 
                              (<Text style={{ color: "white" }}>{ errors[input.name] }</Text>)
                            }
                            </View>))             
                          }           
                          {
                            renderInputs[0].name === 'dateOfBirthConfirm' && 
                            (<View>
                              <DateOfBirth 
                                authChange={ authChange }
                                inputState={ inputState }
                              />
                              <View>
                                <CheckInput 
                                  onClick={ () => { 
                                    authChange({ 
                                      name: renderInputs[0].name, 
                                      value: !inputState[renderInputs[0].name]
                                    });
                                    handleChange(!inputState[renderInputs[0].name])
                                  }}
                                  isChecked={ inputState[renderInputs[0].name] }
                                  leftText="I legally confirm my birthday."
                                  leftTextStyle={{ color: 'white' }}
                                  checkBoxColor="white"
                                />
                              </View>
                              {
                                touched[renderInputs[0].name] && errors[renderInputs[0].name] && 
                                (<Text style={{ color: "white" }}>{ errors[renderInputs[0].name] }</Text>)
                              }
                            </View>) 
                          }
                        <Button onPress={ handleSubmit }>{ buttonName }</Button>
                    </View> 
                  )}}  
            </Formik>)
          }
          {
            renderInputs[0].name === 'confirmation' && (<CodeVerification
              inputState={ inputState }
              userCode={ userCode }
              authChange={ authChange }
            />)
          }
          {
            (!isEmailPhone && renderInputs[0].name !== 'confirmation') &&  
            (<Button onPress={ () => handleBackButton(renderInputs[0].name, setUserInput, inputState) }>
                Back
            </Button>)
          }
      </View>
    </FormView>);
}

const mapStateToProps = ({ inputState }) => { 
  const { birthDayYear, birthDayMonth, birthDayDate, ...noA } = inputState;
  return { 
    inputState, 
    signupInput: {
      ...noA,
      dateOfBirth: `${birthDayMonth} ${birthDayDate} ${birthDayYear}`
    } 
  }; 
}

export default graphql(mutations)(
  connect(mapStateToProps, { authChange })(AuthForm)
) 

const FormView = styled.View`
  background: ${props => props.theme.black};
`;
