import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { Actions } from 'react-native-router-flux'
import styled from 'styled-components/native'
import SignupButton from '../../UI/Buttons/signup.button'

const OuterView = styled.View`
  /* background: red; */
  display: flex;
  align-items: center;
`;

const SignupText = styled.Text`
  /* align-items: center; */
  font-size: ${props => props.theme.h2};
  color: ${props => props.theme.white};
  font-weight: ${props => props.theme.FW200};
  margin-bottom: 20px;
`;

export default class Signupfriends extends Component {
  render() {
    return (
      <OuterView>
        <SignupText>Sign up and see your friends{"\n"}at this bar tonight. </SignupText>
          <SignupButton
          text='Create Account'
          textColor="#ffffff"
          bgColor="#FF7000"
          onPress={() => Actions.signupflow()}
          />
      </OuterView>
    )
  }
}
