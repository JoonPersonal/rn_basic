import React, { Component } from 'react'
import { Feather } from '@expo/vector-icons';

import styled from 'styled-components/native'

const OuterView = styled.View`
  background: black;
  flex-direction: row;
  height: 45px; width: 95%;
  border-radius: 10px;
  margin-top: 10px;
  align-self: center;
 `;
const SearchInput = styled.TextInput`
  height: 35px; width: 90%;
  margin-top: 4px; margin-left: 5px;
  color: ${props => props.theme.informationColor};
  font-size: 18px;
 `;

export default class Search extends Component {
  constructor(props) {
    super(props);
    this.state = { text: 'Search' };
  }
  render() {
    return (
      <OuterView>
        <Feather style={{ lineHeight: 45, marginLeft: 10 }} name="search" size={23} color="#545454" />
        <SearchInput
          onChangeText={(text) => this.setState({ text })}
          value={this.state.text}
        />
      </OuterView>
    )
  }
}
