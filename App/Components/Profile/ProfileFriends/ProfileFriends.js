import React, { Component } from 'react'
import { Text, View, FlatList, Dimensions } from 'react-native'
import styled from 'styled-components/native'

import ProfileFriendsHeader from './ProfileFriendsHeader/ProfileFriendsHeader'
import ProfileFriendsItem from './ProfileFriendsItem/ProfileFriendsItem'
import ProfileFriendsInvisibleItem from './ProfileFriendsItem/ProfileFriendsInvisibleItem'

const data = [
  { key: 'A' }, { key: 'B' }, { key: 'C' }, { key: 'D' }, { key: 'E' }, { key: 'F' }, { key: 'G' }, { key: 'H' }, { key: 'I' }, { key: 'J' }, { key: 'K' },
];

const formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
  while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
    data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
    numberOfElementsLastRow++;
  }

  return data;
};

const OuterView = styled.View`
  /* background: #000; */
  margin-left: 5;
  margin-top: 10;
  height: 100%;
`;

const numColumns = 3

export default class ProfileFriends extends Component {
  _renderItem = ({ item, index }) => {
    if (item.empty === true) {
      return <ProfileFriendsInvisibleItem />
    }
    return (
      <ProfileFriendsItem item={item} />
    );
  };
  render() {
    return (
      <OuterView>
        <ProfileFriendsHeader 
        title="Friends"/>
        <FlatList
        data={formatData(data, numColumns)}
        style={{ flex: 1, marginVertical: 20,}}
        renderItem={this._renderItem}
        numColumns={3}
      />
      </OuterView>
    )
  }
}
