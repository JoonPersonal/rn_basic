import React, { Component } from 'react'
import { Text, Dimensions, Image } from 'react-native'
import styled from 'styled-components'
import { Actions } from 'react-native-router-flux'
import ProfileFriendsHeader from '../ProfileFriendsHeader/ProfileFriendsHeader'
import { TouchableOpacity } from 'react-native-gesture-handler'

const numColumns = 3

// const height = Dimensions.get('window').width / numColumns,

const OuterView = styled.View`
  /* background: red; */
  flex: 1;
  margin: 5px;
  justify-content: center;
  align-items: center;
`;

export default class ProfileFriendsItem extends Component {
  render() {
    const { item } = this.props
    return (
      <OuterView 
        style={{height: Dimensions.get('window').width / numColumns}}
      >
        <TouchableOpacity
          onPress={() => Actions.messageboard()}
        >
          <Image 
            style={{height: 80, width: 80, backgroundColor: 'blue', borderRadius: 40 }} 
            />
          <Text 
            style={{marginTop:5, color: 'white', textAlign: 'center'}}
            >
            Name
          </Text>
          <Text
          style={{color: 'white', textAlign: 'center'}}
          >
            Current Bar
          </Text>
        </TouchableOpacity>
      </OuterView>
    )
  }
}


// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     marginVertical: 20,
//   },
//   item: {
//     backgroundColor: '#4D243D',
//     alignItems: 'center',
//     justifyContent: 'center',
//     flex: 1,
//     margin: 1,
//     height: Dimensions.get('window').width / numColumns, // approximate a square
//   },
//   itemInvisible: {
//     backgroundColor: 'transparent',
//   },
//   itemText: {
//     color: '#fff',
//   },
// });