import React, { Component } from 'react'
import { View, Image, TouchableOpacity } from 'react-native'
import picture from '../../../../deletableAssests/me.png'

export default class ProfileImage extends Component {
  render() {
    return (
      <View style={{ alignSelf: 'center', marginTop: 5 , marginBottom: 25 }}>
        <TouchableOpacity>
          <Image
            style={{ alignSelf: 'center', height: 350, width: 350, borderRadius: 25}}
            source={picture}
          />
        </TouchableOpacity>
      </View>
    )
  }
}

