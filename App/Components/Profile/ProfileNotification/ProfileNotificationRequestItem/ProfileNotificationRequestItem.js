import React, { Component } from 'react'
import { TouchableOpacity, View } from 'react-native'
import styled from 'styled-components/native'
import { LinearGradient } from 'expo-linear-gradient'
import NotificationBubble from '../../../UI/NotificationBubble/NotificationBubble'

const OuterView = styled.View`
  /* background: ${props => props.theme.transparent}; */
  flex-direction: column;
  width: 185px;
  /* setHeight inthe linearGradient */
  margin: 2px;
  margin-top: 25px;
`;

const ContentView = styled.View`
  display: flex;
  height: 100%;
  flex-direction: column;
  justify-content: space-around;
`;

const Image = styled.Image`
  background: ${props => props.theme.black};;
  align-self: center;
  height: 150px;
  width: 150px;
  margin-top: 5;
  border-radius: 75;
`;

const NameandTypeView = styled.View`
  /* margin-top: 5px; */
  display: flex;
  align-self: center;
  flex-direction: row-reverse;
`;

const NameView = styled.View`

`;
const NameText = styled.Text`
  color: ${props => props.theme.contrast};
  line-height: 27;
  text-align: left;
  align-self: flex-end;
  font-size: ${props => props.theme.h5};
  font-weight: 400;
  text-transform: capitalize;
`;


export default class NotificationFriendRequestItem extends Component {
  render() {
    const { notificationRequestData } = this.props
    return (
      <TouchableOpacity>
        <OuterView>
          <LinearGradient
            style={{  height: 200, borderRadius: 5 }}
            colors={['#1d1d1d', '#1d1d1d']}
          >
            <ContentView>
              <Image />
              <NameandTypeView>
                <NotificationBubble color={'orange'} />
                <NameView>
                <NameText> {notificationRequestData} </NameText>
                </NameView>
              </NameandTypeView>
            </ContentView>
          </LinearGradient>
        </OuterView>
      </TouchableOpacity>
    )
  }
}
