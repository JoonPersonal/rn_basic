import React, { Component } from 'react'
import styled from 'styled-components/native'
import { Text } from 'react-native'

const Title = styled.Text`
  position: absolute;
  top: 0; left: 10;
  font-weight: 600;
  font-size: ${props => props.theme.h3};
  color: ${props => props.theme.contrast};
`;

export default class ProfileNotificationHeader extends Component {
  render() {
    const { title } = this.props
    return (
      <Title>
        <Text>{title}</Text>
      </Title>
    )
  }
}
