import React, { Component } from 'react'
import { SectionList } from 'react-native'
import styled from 'styled-components/native'

import ProfileNotificationHeader from './ProfileNotificationHeader/ProfileNotificationHeader'
import ProfileNotificationRequestItem from './ProfileNotificationRequestItem/ProfileNotificationRequestItem'

const OuterView = styled.View`
  height: 270;
  margin-top: 5;
  margin-left: 5;
  width: 100%;
  justify-content: center;
`;

const notificationRequestData = ['Christianospps', 'Joon','Michelle', 'Alexander']

export default class Notification extends Component {
  render() {
    return (
      <OuterView>
        <ProfileNotificationHeader title="Notifications" />
        <SectionList
          horizontal
          showsHorizontalScrollIndicator={false}
          renderItem={({item, index, section}) => 
          <ProfileNotificationRequestItem key={index} notificationRequestData={item} />}
          sections={[
            { title: 'Notifications', data: notificationRequestData },
          ]}
          keyExtractor={(item, index) => item + index}
        />
      </OuterView>
    )
  }
}
