import React, { Component } from 'react'
import { LinearGradient } from 'expo-linear-gradient';
import styled from 'styled-components/native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import ProfileEmojiMoods from './ProfileEmojiMoods/ProfileEmojiMoods'
import ProfileImage from './ProfileImage/ProfileImage'
import ProfileName from './ProfileName/ProfileName'
import ProfileNotification from './ProfileNotification/ProfileNotification'
import ProfileFriends from './ProfileFriends/ProfileFriends'

const OuterView = styled.View`
  height: 100%;
 `;

export default class Profile extends Component {
  render() {
    return (
      <OuterView>
        <LinearGradient
          style={{ flex: 1 }}
          colors={['#0053FF', '#1d1d1d']}
          >
          <KeyboardAwareScrollView>
            <ProfileEmojiMoods />
            <ProfileName />
            <ProfileImage />
            <ProfileNotification />
            <ProfileFriends />
          </KeyboardAwareScrollView>
        </LinearGradient>
      </OuterView>
    )
  }
}

