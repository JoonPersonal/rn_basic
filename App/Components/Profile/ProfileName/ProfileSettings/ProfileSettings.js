import React, { Component } from 'react'
import { TouchableOpacity } from 'react-native'
import { EvilIcons } from '@expo/vector-icons'

export default class ProfileSettings extends Component {
  render() {
    return (
    <TouchableOpacity>
      <EvilIcons name="gear" size={30} color="white" />
    </TouchableOpacity>
    )
  }
}
