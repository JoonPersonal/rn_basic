import React, { Component } from 'react'
import styled from 'styled-components/native'

import ProfileSettings from './ProfileSettings/ProfileSettings'

const OuterView = styled.View`
  justify-content: center;
  margin-top: 15;
  flex-direction: row;
  width: 100%;
`;
const NameTextInput = styled.TextInput`
  width: 300;
  color: ${props => props.theme.contrast};
  font-size: 22;
  line-height: 25;
`;


export default class ProfileName extends Component {
  render() {
    return (
      <OuterView>
        <NameTextInput
          keyboardAppearance='dark'
          returnKeyType='done'
        >
          Christian
        </NameTextInput>
        <ProfileSettings/>
      </OuterView>
    )
  }
}