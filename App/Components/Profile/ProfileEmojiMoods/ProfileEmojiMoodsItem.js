import React, { Component } from 'react'
import { LinearGradient } from 'expo-linear-gradient'
import { TouchableOpacity } from 'react-native'
import styled from 'styled-components/native'

const OuterView = styled.View`
  margin: 2px;
  flex-direction: column;
  align-items: center;
  height: 110;
  width: 85;
`;

const ImageBackground = styled.View`
  height: 80;
  width: 80;
`;
const CircleContainer = styled.View`
  height: 100%;
  width: 100%;
`;
const EmojiText = styled.Text`
  font-size: 43;
  line-height: 80;
  text-align: center;
`;

const Text = styled.Text`
  margin-top: 4;
  height: 100%;
  line-height: 13;
  text-align: center;
  letter-spacing: 0.5;
  color: ${props => props.theme.contrast};
`;

export default class ProfileEmojiMoodsItem extends Component {
  render() {
    const { data } = this.props
    return (
    <OuterView>
      <TouchableOpacity>
        <ImageBackground>
          <LinearGradient
            style={{  borderRadius: 40 }}
            colors={['#fff700', '#7400FF']}
            >
            <CircleContainer>
              <EmojiText>🥰</EmojiText>
            </CircleContainer>
          </LinearGradient>
        </ImageBackground>
        <Text> {data.key} </Text>
      </TouchableOpacity>
    </OuterView>
    )
  }
}
