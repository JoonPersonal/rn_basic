import React, { Component } from 'react'
import { Text, FlatList } from 'react-native'
import styled from 'styled-components/native'
import EmojiMoodsItem from './ProfileEmojiMoodsItem'

const OuterView = styled.View`
  margin-top: 40px;
  height: 110;
  width: 100%;
`;

const EmojiMood = styled.View`
  height: 100%;
  width: 100%;
`;
export default class EmojiMoods extends Component {
  render() {
    return (
      <OuterView>
        <EmojiMood>
          <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal
            data={[{ key: 'awewewe' }, { key: 'bwewewe' }, { key: 'cwewewe' }, { key: 'wwwwww wwwwww' }, { key: 'br' }, { key: 'b2' }]}
            renderItem={({ item }) => <EmojiMoodsItem data={item} />}
          />
        </EmojiMood>
      </OuterView>
    )
  }
}
