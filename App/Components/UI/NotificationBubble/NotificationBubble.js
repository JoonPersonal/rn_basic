import React, { Component } from 'react'
import styled from 'styled-components'
import { Feather, MaterialIcons } from '@expo/vector-icons';

const OuterView = styled.View`
  height: 18px;
  width: 20px;
  border-radius: 13;
  margin-top: 3px;
  margin-right: 3px;
`

export default class NotificationBubble extends Component {
  render() {
    return (
      <OuterView
        style={{backgroundColor: '#FFFFFF00' }}
      >
       {/* <Feather 
        name="message-square" 
        size={20}
        color="white"
       /> */}
       <MaterialIcons 
        name="person-add" 
        size={20}
        color="white"
       />
      </OuterView>
    )
  }
}
