import React, { Component } from 'react'
import { View, DatePickerIOS } from "react-native"
import styled from 'styled-components/native'
// import DatePicker from 'react-native-date-picker'

 const DatePicker = styled.DatePickerIOS`
  background: blue;
  position: absolute;
  bottom: 0; left: 0; right: 0;
  height: 200px;
  font-size: 17px;
  text-align: center;
  color: white;
 `

export default class DatePickerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDateTimePickerVisible: true,
      birthday: '',
      chosenDate: new Date()
    }
    this.setDate = this.setDate.bind(this)
  }

  setDate(newDate) {
    this.setState({chosenDate: newDate})
  }

  // showDateTimePicker = () => {
  //   this.setState({ isDateTimePickerVisible: true })
  // }

  // hideDateTimePicker = () => {
  //   this.setState({ isDateTimePickerVisible: false })
  // }

  // handleDatePicked = date => {
  //   console.log("A date has been picked: ", date)
  //   this.hideDateTimePicker()
  // }


  render() {
    return (
      <>
        {/* <DateTimePicker
          customTitleContainerIOS={ <BirthdayTitle> What's your birthday?</BirthdayTitle>}
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
        /> */}
          <DatePicker
            mode={'date'}
            date={this.state.chosenDate}
            onDateChange={this.setDate}
            maximumDate={new Date(2000,0,1)}
            />
      </>
    )
  }
}

