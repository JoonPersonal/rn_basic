import React, { Component } from 'react'
import styled from 'styled-components/native'

const Decline = styled.View`
  align-self: center;
  background: ${props => props.theme.transparent};
  height: 30;
  width: 100;
  border-radius: 5;
`

const Text = styled.Text`
  color: ${props => props.theme.contrast};
  text-align: center;
  line-height: 30;
  /* font-size: ${props => props.theme.informationFontSize}; */
  font-size: 15;
  font-weight: 500;
  letter-spacing: 0.2;
`

export default class DeclineButton extends Component {
  render() {
    return (
      <Decline>
        <Text> Decline</Text>
      </Decline>
    )
  }
}
