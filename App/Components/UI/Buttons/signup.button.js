import React, { Component } from 'react'
import styled from 'styled-components/native'
import PropTypes from 'prop-types'

const ButtonSignup = styled.TouchableOpacity`
  /* Change signup button bgcolor after form is valid */
  background: ${props => props.theme.orange};
  width: 300;
  border-radius: 4;
`

const TextSignup = styled.Text`
  color: ${props => props.theme.contrast};
  text-align: center;
  line-height: 42;
  font-size: ${props => props.theme.h5};
  font-weight: ${props => props.theme.FW600};
`

export default class RoundButton extends Component {
  render() {
    const { text, onPress, height } = this.props
    const nHeight = height === undefined ? 42 : height

    return (
      <ButtonSignup 
        style={{ height: nHeight }}
        onPress={onPress}
      >
        <TextSignup>{text}</TextSignup>
      </ButtonSignup>
    )
  }
}
RoundButton.protoTypes = {
  text: PropTypes.string.isRequired,
  bgColor: PropTypes.string.isRequired,
  textColor: PropTypes.string.isRequired,
  onPress: PropTypes.func,
}


