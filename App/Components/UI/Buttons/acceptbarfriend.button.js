import React, { Component } from 'react'
import styled from 'styled-components/native'
import AcceptButton from './accept.button'
import DeclineButton from './decline.button'

const OuterView = styled.View`
  position: absolute;
  top: 10;
  right: 0;
  width: 220;
  flex-direction: row;
`

export default class AcceptbarfriendButton extends Component {
  render() {
    return (
      <OuterView>
        <DeclineButton />
        <AcceptButton />
      </OuterView>
    )
  }
}
