import React, { Component } from 'react'
import { Router, Scene } from 'react-native-router-flux'

import TestComponent from './TestComponent'
import Map from './Map/Map'
// CHANGES
{/* Profile */ }
import Profile from './Profile/Profile'

{/* Messaging */ }
import MessageBoard from './MessageBoard/MessageBoard'

{/* Credentials */ }
import AuthForm from './Credentials/Authentications/AuthUser/AuthForm';

// import MyReactNativeForm from '../Components/Credentials/SignUp/Common/Input';

export default class NavigationRouter extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Router>
        <Scene key="root" hideNavBar={true}>
          <Scene key="testcomponent" component={TestComponent} />
          {/* Sign Up */}
          <Scene key="authForm" initial component={ AuthForm } />  

          {/* Map Main initial screen */}
          <Scene key="map" component={Map} />

          {/* Profile */}
          <Scene key="profile" component={Profile} />

          {/* Messaging */}
          <Scene key="messageboard" component={MessageBoard} />
        </Scene>
      </Router>
    )
  }
}
