const Theme = {
  'orange': '#FF7000',
  'white': '#FFFFFF',
  'darkgrey': '#1d1d1d',
  'black': '#000000',
  'contrast': '#F2F2F2',
  'informationColor': '#3E3E3E',
  'transparent': '#FFFFFF00',
  'h5': '17px',
  'h4': '19px',
  'h3': '21px',
  'h2': '25px',
  'h1': '27px',
  'FW200': '200',
  'FW400': '400',
  'FW500': '500',
  'FW600': '600',
  'FW800': '800',
}

export default Theme;