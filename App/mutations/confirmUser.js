import gql from 'graphql-tag';

export default gql`
    mutation CodeVerification(
        $isVerified: Boolean!
    ) {
        codeVerification(
            isVerified: $isVerified
        ) { 
            phone
            email
            token
        }  
    }
`;
