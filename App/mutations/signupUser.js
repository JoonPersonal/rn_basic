import gql from 'graphql-tag';

export default gql`
    mutation VerificationCodeCreation(
        $phone: String,
        $email: String,
        $firstName: String!, 
        $lastName: String!, 
        $dateOfBirth: String!, 
        $dateOfBirthConfirm: Boolean!,
        $password: String!,
        
    ) {
        verificationCodeCreation(data: {
            phone: $phone
            email: $email
            firstName: $firstName
            lastName: $lastName
            dateOfBirth: $dateOfBirth
            dateOfBirthConfirm: $dateOfBirthConfirm
            password: $password
        }) {
            smsCode
            user {
                email
                phone
            }
        }  
    }
`;
